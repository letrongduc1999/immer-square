import React, {useMemo, useEffect} from "react";
import { Button, Box } from "theme-ui";
import RenderBox from "./Box";

const Canvas = ({ boxes, moveElement, selectedBox, dispatch, resizeElement }) => {
  const [stateBoxes, setStateBoxes] = React.useState(boxes)

  useEffect(() => {
    setStateBoxes(boxes)
    console.log(selectedBox)
  }, [boxes])


  return (
    <Box
      sx={{
        width: "80%",
        backgroundColor: "white",
        height: "100vh",
        position: "relative",
        overflow: "hidden"
      }}
    >
      {Object.values(stateBoxes).map((box) => (
        <RenderBox box={box} dispatch={dispatch} resizeElement={resizeElement} moveElement={moveElement} selectedBox={selectedBox}/>
      ))}
    </Box>
  );
};

export default Canvas;
